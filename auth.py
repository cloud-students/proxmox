from flask_login import (
    LoginManager,
    UserMixin,
    current_user
)
from flask import redirect, url_for

from lib.account import get_all_user, get_user, role


class User(UserMixin):
    def __init__(self, account):
        user = get_user(account)
        
        self.id = user["account"]
        self.identity = role[user["identity"]]
        if self.identity == "supervisor":
            self.name = "supervisor"
        else:
            self.name = user["name"]

login_manager = LoginManager()

@login_manager.user_loader
def user_loader(account):
    if account not in get_all_user():
        return 
    
    user = User(account)
    
    return user

@login_manager.request_loader
def request_loader(request):
    account = request.form.get('user_account')
    if account not in get_all_user():
        return
    
    user = User()
    user.is_authenticates = True
    #user.is_authenticates = request.form['password'] == 
    return user

@login_manager.unauthorized_handler
def unauthorize_callback():
    return redirect(url_for("account_web.login"))
