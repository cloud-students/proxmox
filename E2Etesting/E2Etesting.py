from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import time
import unittest
class FooriendTest(unittest.TestCase):
    def setUp(self):
        options=Options()
        options.add_argument("--disable-notifications")
        options.add_argument('--headless')
        #self.chrome = webdriver.Chrome('./chromedriver',chrome_options=options)
        #self.chrome = webdriver.Chrome('/usr/bin/chromedriver',chrome_options=options)
        
        self.chrome = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=options)
        self.addCleanup(self.chrome.quit)
    def testLogin(self):    
        self.chrome.get('http://192.168.0.103/web/account/login')
#chrome.get("https://www.google.com")
        account=self.chrome.find_element_by_id("account")
        password=self.chrome.find_element_by_id("password")
        account.send_keys('charles')
        password.send_keys('cloud1234')
        password.submit()
        time.sleep(3)
        grouplist=self.chrome.find_element_by_id("group_list")
    def testLoginFail(self):
        self.chrome.get('http://192.168.0.103/web/account/login')
        account=self.chrome.find_element_by_id("account")
        password=self.chrome.find_element_by_id("password")
        account.send_keys('testfail')
        password.send_keys('wrongwrong')
        password.submit()
        time.sleep(3)
        try:
            grouplist=self.chrome.find_element_by_id("group_list")
        except:
            return
        self.fail("It should be fail login.")
    def testGetGrouplist(self):
        self.chrome.get('http://192.168.0.103/web/account/login')
        account=self.chrome.find_element_by_id("account")
        password=self.chrome.find_element_by_id("password")
        account.send_keys('charles')
        password.send_keys('cloud1234')
        password.submit()
        time.sleep(3)
        rows=self.chrome.find_elements_by_xpath("//table/tbody/tr")
        try:
            print(len(rows))
        except:
            self.fail("No Group List Exist.")
if __name__=='__main__':
    unittest.main(verbosity=2)
