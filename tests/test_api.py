import pytest
import json
import requests
from datetime import datetime

home_url = 'http://192.168.103:80'

def test_valid_login():
    info = {"account": "charles", "password": "cloud1234"}
    url = home_url + '/web/account/login'
    r = requests.post(url, data = info)
    assert r.status_code == 200
    # assert {"role": 1} == json.loads(response.data)

def test_invalid_login():
    info = {"account": "charles", "password": "hi"}
    url = home_url + '/web/account/login'
    r = requests.post(url, data = info)
    #response = .post('/account/login',
    #                           json=info)
    assert r.status_code == 400

def test_get_customer():
    url = home_url + '/customer/'
    #response = .get('/customer/')
    r = requests.get(url)
    assert r.status_code == 200
    # assert b'username' in r.data
    # assert b'gender' in r.data
    # assert b'phone' in r.data

def test_get_supervisor():
    #response = .get('/supervisor/')
    url = home_url + '/supervisor/'
    r = requests.get(url)
    assert r.status_code == 200
    # assert b'account' in r.data
    # assert b'identity' in r.data

def test_get_group():
    #response = .get('/group/')
    url = home_url + '/groups/'
    r = requests.get(url)
    assert r.status_code == 200

def test_post_valid_group():
    info = {"main_user": "charles", "num_people": 3, "time": "2021-11-11 17:00:00", "restaurant": "dinner", "order_content": "coke"}
    #response = .post('/group/',
    #                            json=info)
    url = home_url + '/groups/'
    r = requests.post(url, data = info)
    #assert r.status_code == 200    #wrong!!!!!
    assert r.status_code == 400
    # assert b'group_id' in r.data

def test_post_invalid_group():
    info = {"main_user": "test", "num_people": 3}
    #response = .post('/group/',
    #                            json=info)
    url = home_url + '/groups/'
    r = requests.post(url, data = info)
    assert r.status_code == 400

def test_join_invalid_group():
    info = {"user_name": "test", "group_id": -1234, "order_content": "food"}
    #response = .post('/group/-1234',
     #                           json=info)
    url = home_url + '/groups/-1234'
    r = requests.post(url, data = info)
    assert r.status_code == 400
