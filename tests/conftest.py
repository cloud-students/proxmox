import pytest
import main

@pytest.fixture(scope='module')
def test_client():
    return main.app.test_client()
        