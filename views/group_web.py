from flask import (
    Blueprint,
    request,
    render_template,
    url_for,
    redirect,
    flash,
    json
)
from flask_sqlalchemy import SQLAlchemy
from database import db
from flask_login import login_user, current_user, logout_user, login_required

from auth import User
from lib.account import role, get_all_user
import views.customer_api as customer_usage


group_web = Blueprint("group_web", __name__)

@group_web.route("/list")
@login_required
def showGroup():
    result = "" 
    status = ""

    result, status = customer_usage.search(current_user.name)

    result = (json.loads(result.get_data().decode("utf-8")))[0]
   
    return render_template("group_list.html", user_info=result, auth=current_user.identity)
