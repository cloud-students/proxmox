from flask import Blueprint, jsonify, request
from database import db
import random
import traceback
from datetime import datetime, timedelta

from flask_login import login_user, login_required, current_user
#from auth import User


order_api = Blueprint('order_api', __name__)

@order_api.route("/<account>", methods=["POST"]) #customer order
@login_required
def order(account):
    try:
        data = request.get_json()
        id = int(random.random()*1000000)
        db.engine.execute("Insert INTO `Order_meal` values(%s,%s,%s,%s,%s,%s)",\
           (id, account, data["content"],0,data["restaurant"], data["time"]))
        return jsonify({'order_id':id}),200
    except:
        return jsonify({"response":'error'})

@order_api.route("/")
@login_required
def get_order():
    if current_user.identity == "customer":
        orders = db.engine.execute("Select * from `Order_meal` inner join `Groups` on (`Order_meal`.id= `Groups`.group_id and `user`=%s)",current_user.name)
        joined_orders = db.engine.execute("Select * from `Order_meal` inner join `Groups` on (`Order_meal`.id= `Groups`.group_id and `user`!=%s)",current_user.name)
       
    elif current_user.identity == "restaurant":
        orders = db.engine.execute("Select * from `Order_meal` inner join `Groups` on (`Order_meal`.id= `Groups`.group_id and `Order_meal`.`restaurant`=%s)",current_user.name)
    else:
        return "something error"
    result = []
    try:
        for order in orders:
            if order["time"] < (datetime.now() - timedelta(minutes=60)):
               continue
            tmp = {}
            tmp["id"] = order["id"]
            tmp["username"] = order["user"]
            tmp["is_accept"] = order["is_accept"]
            tmp["restaurant"] = order["restaurant"]
            tmp["time"] = order["time"]
            tmp["content"] = order["order_content"]
            tmp["joined_user"] = order["joined_user"]
            result.append(tmp)
        if current_user.identity == "customer":  
            for order in joined_orders:
                if order["time"] < (datetime.now() - timedelta(minutes=60)):
                    continue
                if current_user.name in order["joined_user"].split(','):
                    tmp = {}
                    tmp["id"] = order["id"]
                    tmp["username"] = order["user"]
                    tmp["is_accept"] = order["is_accept"]
                    tmp["restaurant"] = order["restaurant"]
                    tmp["time"] = order["time"]
                    tmp["content"] = order["order_content"]
                    tmp["joined_user"] = order["joined_user"]
                    result.append(tmp)
        return jsonify(result), 200
    except:
        return jsonify({"response":'error'}),400

@order_api.route("/accept", methods=['POST'])
@login_required
def accept_order():
    if current_user.identity != "restaurant":
        return jsonify({"response": 'Permission denied'}), 401
    try:
        data = request.get_json()
        order = db.engine.execute("Select * from `Order_meal` where `id`=%s", data["order_id"])
        for item in order: # only one item
            if item["restaurant"] != current_user.name:
                return jsonify({"response": "Permission denied"}), 401
            db.engine.execute("Update `Order_meal` set `is_accept`=%s where `id`=%s",(1, item["id"]))
    except Exception as exception:
        traceback.print_exc()
        return jsonify({"response": "something wrong"}), 400

    return jsonify({"response": 'success'}), 200
			

