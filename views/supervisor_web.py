from flask import Blueprint, json, render_template, url_for
from flask_login import login_required, current_user
from database import db

from views.supervisor_api import show 
supervisor_web = Blueprint('supervisor_web', __name__)


@supervisor_web.route("/")
@login_required
def edit_page():
    if(current_user.identity != "supervisor"):
        return "Permission denied"
    result, status = show()
    result = json.loads(result.get_data().decode("utf-8"))
    
    return render_template("supervisor.html", accounts=result)



