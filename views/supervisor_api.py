from flask import Blueprint, jsonify, request
from flask_login import login_required, current_user
from database import db

supervisor_api = Blueprint('supervisor_api', __name__)

@supervisor_api.route("/", methods=['GET'])
@login_required
def show():
    sql_cmd = """ select * from `Account_Manage` """
    query_data = db.engine.execute(sql_cmd)
    accounts = []
    for row in query_data: 
        account = {}
        account['account'] = row['account']
        account['identity'] = row['identity']
        accounts.append(account)
    return jsonify(accounts),200
   
@supervisor_api.route("/<account>",methods=['DELETE'])
@login_required
def r_edit(account):
    if current_user.identity != "supervisor":
        return jsonify({"response": "Permission failed"}), 401
    try:
        db.engine.execute("Delete From `Account_Manage` where `account`=%s",(account))
        return jsonify({"response":'success'}),200
    except:
        return jsonify({"response":'failed'}),400
