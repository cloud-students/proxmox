from flask import Blueprint, json, request, jsonify, render_template
from database import db
from datetime import datetime, timedelta
import time
import random
from flask_login import current_user
from lib.notify import notify_by_mail
group_api = Blueprint('group_api', __name__)


@group_api.route("/", methods=["GET","POST"])# show group list 
def group():
    if request.method == "GET":
        sql_cmd = """ select * from `Groups` """ 
        query_data = db.engine.execute(sql_cmd)
        my_groups = []
        other_groups = [] 
        for row in query_data: 
            if current_user.name in row['joined_user'].split(','):
                if row['time'] > datetime.now():
                    group = {}
                    group['group_id'] = row['group_id']
                    group['main_user'] = row['main_user']
                    group['num_people'] = row['num_people']
                    group['joined_user'] = row['joined_user']
                    group['time'] = row['time']
                    group['restaurant'] = row['restaurant']
                    group['succeeded'] = row['succeeded']
                    group['order_id'] = row['order_id']
                    group['order_content'] = row['order_content']
                    my_groups.append(group) 
            else:
                if row['time'] > datetime.now():
                    group = {}
                    group['group_id'] = row['group_id']
                    group['main_user'] = row['main_user']
                    group['num_people'] = row['num_people']
                    group['joined_user'] = row['joined_user']
                    group['time'] = row['time']
                    group['restaurant'] = row['restaurant']
                    group['succeeded'] = row['succeeded']
                    group['order_id'] = row['order_id']
                    group['order_content'] = row['order_content']
                    other_groups.append(group)
        groups = {}
        groups['my_groups'] = my_groups
        groups['other_groups'] = other_groups
        return jsonify(groups)#render_template('/home/cloud/flask_app/templates/group.html',title=title)
    if request.method == "POST":
        try:
            id = -1
            sql_cmd = """ select * from `Groups` """ 
            query_data = db.engine.execute(sql_cmd)
            for row in query_data: 
                if int(row['group_id']) > id:
                    id = int(row['group_id'])
            id = id + 1
            data = request.get_json()
            data["time"] = str(data["time"]).replace("T"," ")
            db.engine.execute("Insert INTO `Groups` values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
            (id, data["main_user"], data["num_people"],data["main_user"],\
            data["time"], data["restaurant"],0 ,'', data["order_content"]))
            return jsonify({'group_id':id}),200
        except:
            return jsonify({"response":'error'}),400
        
@group_api.route("/<id>", methods=['POST'])
def joingroup(id):
    try:
        data = request.get_json()  #get user_name, group_id, order_content  
        try:
            sql_cmd = """ select `time`,`joined_user` from `Groups` """
            query_data = db.engine.execute(sql_cmd)
            rowdata = db.engine.execute("Select * from `Groups` where `group_id` = %s",(id))
        except:
            return jsonify({"response":'DBerror'}),400
        
        num_peo = 0
        joined_peo = []
        succeed = 0
        order_content = []
        main_user = 'x'
        _time = 0
        restaurant = 'x'
        my_groups_time = [] # store datetime of own groups
        for row in query_data:
            if current_user.name in row['joined_user'].split(','):
                my_groups_time.append(row["time"])
        for row in rowdata:
            for dt in my_groups_time:
                if dt-timedelta(hours=1) < row['time'] < dt+timedelta(hours=1):
                    return jsonify({"response":'time conflict'}),300
            main_user = row['main_user']
            num_peo = int(row['num_people'])
            joined_peo = row['joined_user'].split(',')
            _time = row['time']
            restaurant = row['restaurant']
            succeed = int(row['succeeded'])
            order_content = row['order_content'].split(',')
            #add joined user and order_content   
            
            joined_peo.append(data["user_name"])
            order_content.append(data["order_content"])
            
            
            order_content = ','.join(order_content) # `succeeded` = %s `order_content` = %s
            #succeeded? yes -> send menu
            
            if len(joined_peo) == num_peo:
                succeed = 1
                db.engine.execute("Update `Groups` set `order_id` = %s where `group_id` = %s",(id,id))
                db.engine.execute("Update `Groups` set `succeeded` = %s where `group_id` = %s",(succeed,id))
                db.engine.execute("Insert INTO `Order_meal` values(%s,%s,%s,%s,%s)",\
                (id, main_user, 3, restaurant, _time)) # 0:deny, 1:accept, 3:unknow
                notify_by_mail(joined_peo, "group_success_customer", id)
                notify_by_mail(restaurant, "group_success_restaurant", id)
            if len(joined_peo) > num_peo:
                return jsonify({"response":'full'}),40
            joined_peo = ','.join(joined_peo)
            db.engine.execute("Update `Groups` set `joined_user` = %s where `group_id` = %s",(joined_peo,id))
            db.engine.execute("Update `Groups` set `order_content` = %s where `group_id` = %s",(order_content,id))
            return jsonify({"response":'success'}),200#jsonify(data
    except:
        return jsonify({"response":'failed'}) ,400

@group_api.route("/delete", methods=['DELETE'])
def delete():
    data = request.get_json() #get main_user and group_id
    try:
        rowdata = db.engine.execute("Select * from `Groups` where `group_id` = %s",(data['group_id']))
    except:
        return jsonify({"response":'DBerrot'}),400
    for row in rowdata:
        if int(row['succeeded']) == 0 and row['main_user'] == data['main_user']:
            db.engine.execute("Delete from `Groups` where `group_id` = %s",(data['group_id']))
            return jsonify({"response":'success'}),200
        else:
            return jsonify({"response":'the group is succeeded'}),400
