from flask import Blueprint,jsonify,request
from database import db

restaurant_api = Blueprint('restaurant_api', __name__)

@restaurant_api.route("/")#show all restaurant
def display():
    try:
        rowdata = db.engine.execute("Select * from `Restaurant`")
        restaurants = []
        try:
            for row in rowdata:
                restaurant = {}
                restaurant["username"] = row["name"]
                restaurant["phone"] = row["phone"]
                restaurant["address"] = row["address"]
                restaurant["menu"] = row["menu"]
                restaurant["status"] = row["status"]
                restaurant["mail"] = row["mail"]
                restaurants.append(restaurant)
            return jsonify(restaurants),200
        except:
            return jsonify({"response":'error'}),400
    except:
        return jsonify({"response":'error'}),400

@restaurant_api.route("/<username>")#get restaurant info 
def search(username):
    try:
        rowdata = db.engine.execute("Select * from `Restaurant` where `name`=%s",(username))
        restaurants = []
        try:
            for row in rowdata:
                restaurant = {}
                restaurant["username"] = row["name"]
                restaurant["phone"] = row["phone"]
                restaurant["address"] = row["address"]
                restaurant["menu"] = row["menu"]
                restaurant["status"] = row["status"]
                restaurant["mail"] = row["mail"]
                restaurants.append(restaurant)
            return jsonify(restaurants),200
        except:
            return jsonify({"response":'error'}),400
    except:
        return jsonify({"response":'error'}),400

@restaurant_api.route("/<account>",methods=["POST"])
def set(account):
    try:
        data = request.get_json()
        db.engine.execute("Update `Restaurant` set `name`=%s, `address`=%s, `phone`=%s, `menu`=%s, `status`=%s `mail`=%s  where `account` = %s",(data["username"],data["address"],data["phone"],data["menu"],data["open"], data["mail"], account))
        return jsonify({"response":'success'}),200
    except:
        return jsonify({"response":'failed'}),400
