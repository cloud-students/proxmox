from flask import Blueprint, json, request, jsonify, render_template, url_for
from flask_login import login_required, current_user
from database import db

import views.customer_api as customer_usage
import views.restaurant_api as restaurant_usage
profile_web = Blueprint('profile_web', __name__)


@profile_web.route("/edit")
@login_required
def edit_page():
    result = "" 
    status = ""
    if(current_user.identity == "customer"):
        result, status = customer_usage.search(current_user.name)
    else:
        result, status = restaurant_usage.search(current_user.name)
    result = (json.loads(result.get_data().decode("utf-8")))[0]
    api = current_user.identity+"_api.set"
    _url = url_for(api, account=current_user.id)
    _url = "http://140.113.213.74:8888" + str(_url)
    return render_template("profile_edit.html", user_info=result, URL=_url)

@profile_web.route("/<username>")#get customer info
@login_required 
def search_web(username):
    if not current_user.is_authenticated:
        render_template("login.html", auth="none")
    else:
        result = "" 
        status = ""
        if(current_user.identity == "customer"):
            result, status = customer_usage.search(username)
        else:
            result, status = restaurant_usage.search(username)
        result = (json.loads(result.get_data().decode("utf-8")))[0]
        return render_template("personal_file.html", user=result)

