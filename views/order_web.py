from flask import (
    Blueprint,
    render_template,
    url_for,
    json
)
from flask_login import login_user, current_user, login_required

from views.order_api import get_order 
from lib.account import role, get_all_user

order_web = Blueprint("order_web", __name__)

@order_web.route("/list")
@login_required
def order_list():
    result, status = get_order()
    if status == 400:
        return "API get failed"
    result = json.loads(result.get_data().decode("utf-8"))
    return render_template("order_list.html", orders=result)
