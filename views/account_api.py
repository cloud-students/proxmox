from flask import Blueprint, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from database import db

from flask_login import login_user, logout_user, login_required, current_user
from auth import User

account_api = Blueprint("account_api", __name__)


@account_api.route("/login",methods=["POST"])
def login():
    data = request.get_json()
    rowdata = db.engine.execute("Select * from `Account_Manage`")
    for row in rowdata: #check account
        if row['account'] == data['account']:
            if row['password'] == data['password']:
                user = User(data['account'])
                login_user(user)
                return jsonify({'role':row['identity']}), 200
            else:
                return jsonify({"response":'password error'}), 400
    return jsonify({"response":"account not exist"}), 400

@account_api.route("/logout")
@login_required
def logout():
    logout_user()
    return jsonify({"response":''})

@account_api.route("/register",methods=["GET","POST"])
def register():
     if request.method == 'POST':
         try:
             role = -1 # 1:customer   3: restaurant
             data = request.get_json()
             rowdata = db.engine.execute("Select * from `Account_Manage`")
             print(data)
             for row in rowdata: #check account
                 if row['account'] == data['account']:
                     return jsonify({"response":'account has been used'}),400
             if data['role'] == 'customer':
                 role = 1
                 db.engine.execute("Insert Into `Account_Manage` values(%s,%s,%s)",\
                     (data["account"],data["password"],role))
                 db.engine.execute("Insert Into `Customer` values(%s,%s,%s,%s,%s)",\
                     (data["account"],data["username"],data["gender"],data["phone"],data["mail"]))
               
             if data['role'] == 'restaurant':
                 role = 3
                 print(data)
                 db.engine.execute("Insert Into `Account_Manage` values(%s,%s,%s)",\
                     (data["account"],data["password"],role))
                 db.engine.execute("Insert Into `Restaurant` values(%s,%s,%s,%s,%s,%s,%s)",\
                     (data["account"],data["username"],data['phone'],data['address'],\
                     data['menu'],data['status'],data["mail"]))
             return jsonify({"response":'success'}),200
         except:
             return jsonify({"response":'error'}),400

'''@account_api.route("/test")
def show_data():
    sql_cmd = """ select * from restaurant """
    query_data = db.engine.execute(sql_cmd)
    result = "Data: "
    for row in query_data:
        result += "<br />Name: " + row['name'] + "<br /> ID: "+row["account"] +"<br />"
    return result'''
