from flask import (
    Blueprint,
    request,
    jsonify,
    render_template,
    url_for,
    redirect,
    flash
)
from flask_login import current_user, login_required


restaurant_web = Blueprint("restaurant_web", __name__)

@restaurant_web.route("/list")
@login_required
def showRestaurant():
    return render_template("restaurant_list.html")
