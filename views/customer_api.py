from flask import Blueprint, json, request, jsonify, render_template
from database import db
from flask_login import current_user, login_required
import traceback
customer_api = Blueprint('customer_api', __name__)


@customer_api.route("/")#display customers
@login_required
def show():
    try:
        rowdata = db.engine.execute("Select * from `Customer`")
        customers = []
        for row in rowdata:
            customer = {}
            customer['username'] = row['name']
            customer['gender'] = row['gender']
            customer['phone'] = row['phone']
            customer['mail'] = row['mail']
            customers.append(customer)
        return jsonify(customers),200
    except:
        return jsonify({"response":'failed'}),400

@customer_api.route("/<username>")#get customer info 
def search(username):
    
    if current_user.is_authenticated:
        try:
            rowdata = db.engine.execute("Select * from `Customer` where `name`=%s",(username))
            customers = []
            for row in rowdata:
                customer = {}
                customer['username'] = row['name']
                customer['gender'] = row['gender']
                customer['phone'] = row['phone']
                customer['mail'] = row['mail']
                customers.append(customer)
            return jsonify(customers),200
        except:
            return jsonify({"response":'failed'}),400
    else:
        return jsonify({"response":'failed'}),400

@customer_api.route("/<account>",methods=["POST"])
def set(account):
    try:
        data = request.get_json()
        print(data)
        db.engine.execute("Update `Customer` set `name`=%s, `gender`=%s, `phone`=%s, `mail`=%s where `account` = %s",(data["username"],data["gender"],data["phone"],data["mail"], account))
        return jsonify({"response":'success'}),200
    except Exception as exception:
        traceback.print_exc()
        return jsonify({"response":'failed'}),400
