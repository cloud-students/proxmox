from re import S
from flask import (
    Blueprint,
    request,
    jsonify,
    render_template,
    url_for,
    redirect,
    flash,
    make_response
)
from flask_sqlalchemy import SQLAlchemy
from database import db
from flask_login import login_user, current_user, logout_user, login_required

from auth import User
from lib.account import get_all_user

account_web = Blueprint("account_web", __name__)

@account_web.route("/login",methods=["GET"])
def login():
    return render_template("login.html", auth="none")
    """if not request.form["password"]:
        return render_template("login.html", auth="none")
    users = get_all_user()
    account = request.form["account"]
    if (account in users) and request.form['password'] == users[account]:
        user = User(account)
        login_user(user)
        flash(f"歡迎 {user}! 願你找到 Foodriend!")
        if current_user.identity == "restaurant":
            resp = make_response(render_template("order.html"))
            resp.set_cookie("samesite","Lax")
            return render_template("order.html")
        else:
            resp = make_response(render_template("group_list.html"))
            resp.set_cookie("samesite","Lax")
            return render_template("group_list.html", auth=current_user.identity)
    
    else:
        flash("login failed")
        return render_template("login.html", auth="none", Message="帳號或密碼輸入錯誤"), 400
    """

@account_web.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("account_web.login"))

@account_web.route("/register")
def register():
    return render_template("register.html", auth="none")
@account_web.route("/select_Role")
def select_Role():
    return render_template("select_Role.html",auth="none")

@account_web.route("/")
@login_required
def reroute():
		if current_user.identity == "supervisor":
				return redirect(url_for('supervisor_web.edit_page'))
		elif current_user.identity == "restaurant":
				return redirect(url_for('order_web.order_list'))
		else:
				return redirect(url_for('group_web.showGroup'))
