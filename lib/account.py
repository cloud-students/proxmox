from database import db


role = {1:"customer", 2:"vip", 3:"restaurant", 4:"supervisor"}

def get_all_user():
    users = {}
    rowdata = db.engine.execute("Select * from `Account_Manage`")
    for row in rowdata:
        users[row["account"]] = row["password"]
    return users

def get_user(account):
    user = {}
    rowdata = db.engine.execute("Select * from `Account_Manage` where `account`=%s",(account))
    for row in rowdata:
        user["account"] = row["account"]
        user["identity"] = row["identity"]
        if role[user["identity"]] == "customer":
            user["name"] = get_customer(account)["name"]
        elif role[user["identity"]] == "restaurant":
            user["name"] = get_restaurant(account)["name"]
    return user

def get_customer(account):
    customer = {}
    rowdata = db.engine.execute("Select * from `Customer` where `account`=%s", (account))
    for row in rowdata:
        customer["account"] = row["account"]
        customer["name"] = row["name"]
        customer["gender"] = row["gender"]
        customer["phone"] = row["phone"]
    return customer

def get_customer_by_name(username):
    customer = {}
    rowdata = db.engine.execute("Select * from `Customer` where `name`=%s", (username))
    for row in rowdata:
        customer["account"] = row["account"]
        customer["name"] = row["name"]
        customer["gender"] = row["gender"]
        customer["phone"] = row["phone"]
        customer["mail"] = row["mail"]
    return customer

def get_restaurant(account):
    restaurant = {}
    rowdata = db.engine.execute("Select * from `Restaurant` where `account`=%s", (account))
    for row in rowdata:
        restaurant["account"] = row["account"]
        restaurant["name"] = row["name"]
        restaurant["address"] = row["address"]
        restaurant["menu"] = row["menu"]
        restaurant["status"] = row["status"]
    return restaurant

def get_restaurant_by_name(username):
    restaurant = {}
    rowdata = db.engine.execute("Select * from `Restaurant` where `name`=%s", (username))
    for row in rowdata:
        restaurant["account"] = row["account"]
        restaurant["name"] = row["name"]
        restaurant["address"] = row["address"]
        restaurant["menu"] = row["menu"]
        restaurant["status"] = row["status"]
        restaurant["mail"] = row["mail"]
    return restaurant
