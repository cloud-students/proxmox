from flask_mail import Mail, Message
from config import MAIL_USERNAME
from lib.account import get_customer_by_name, get_restaurant_by_name


#notify_type={
#	0 : 'group_success_customer',
#	1 : 'group_success_restaurant'
#}

mail = Mail()

def send_mail(receiver, notify_subject, message):
		msg = Message(subject=notify_subject,
									sender=MAIL_USERNAME,
									recipients=receiver,
									body=message)
		mail.send(msg)

def notify_by_mail(receivers, _type, group_id):
		message = ""
		subject = ""
		if _type == "group_success_customer":
			for receiver in receivers:
				customer = get_customer_by_name(receiver)
				subject = "Group Success!"
				message = "Hi, {}\n\
									Congrats! Your date is coming! \n\
                  Group ID: {}\n\
									For more details, please check the link below.\n\
									{}\n\
									Wish you have a nice date.\n\
									Best,\n\
									Fooriend".format(customer["name"], group_id,"https://140.113.213.74:8888/web/group/list")
				receiver_mail = customer["mail"]
				send_mail([receiver_mail], subject, message)

		if _type == "group_success_restaurant":
			print("group_success!: " + receivers)
			restaurant = get_restaurant_by_name(receivers)
			print(restaurant)
			subject = "Group Success - Order Content"
			message = "Hi, {}\n訂單來了!\nPlease check the order (ID: {})detail from the link below.\n{}\nBest,\nFooriend".format(restaurant["name"], group_id, "https://140.113.213.74:8888/web/order/list")
			receiver_mail = restaurant["mail"] 
			send_mail([receiver_mail], subject, message)
			

										

