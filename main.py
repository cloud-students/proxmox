from flask import send_from_directory,Flask, Blueprint, redirect, url_for,render_template
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

from database import db
from lib.notify import mail, notify_by_mail
from auth import login_manager


from views import (
    customer_api,
    account_api,
    restaurant_api,
    supervisor_api,
    group_api,
    order_api,
    account_web,
    group_web,
    restaurant_web,
    profile_web,
    order_web,
    supervisor_web
)

def register(app):
    app.register_blueprint(customer_api.customer_api, url_prefix="/customer")
    app.register_blueprint(account_api.account_api, url_prefix="/account")
    app.register_blueprint(restaurant_api.restaurant_api, url_prefix="/restaurant")
    app.register_blueprint(supervisor_api.supervisor_api, url_prefix="/supervisor")
    app.register_blueprint(group_api.group_api, url_prefix="/group")
    app.register_blueprint(order_api.order_api, url_prefix="/order")


def web_register(app):
    app.register_blueprint(account_web.account_web, url_prefix="/web/account")
    app.register_blueprint(profile_web.profile_web, url_prefix="/web/profile")
    app.register_blueprint(group_web.group_web, url_prefix="/web/group")
    app.register_blueprint(restaurant_web.restaurant_web, url_prefix="/web/restaurant")
    app.register_blueprint(order_web.order_web, url_prefix="/web/order")
    app.register_blueprint(supervisor_web.supervisor_web, url_prefix="/web/supervisor")

app = Flask(__name__)
app.config.from_object('config')

login_manager.init_app(app)
login_manager.login_view = 'login.html' 
login_manager.login_message = '請登入系統'

db.init_app(app)
mail.init_app(app)
#CORS(app)
#CORS(app, resouces={r"/.*":{"origins": ["http://192.168.0.103"] }})
register(app)
web_register(app)

@app.route('/')
def hello_world():
	return "Hello world"

@app.route('/web')
def index():
	return redirect(url_for("account_web.login"))

@app.route("/web/push")
def test_push():
	rec_list = ["yu"]
	notify_by_mail(rec_list, "group_success_customer")
	return "Test mail"
	#return render_template("test_push.html")

if __name__ == "__main__":
	app.debug = True
	app.run()
